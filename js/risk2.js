var latUrl;
var lngUrl;
var addUrl;
var distanceUrl;
var map;
var geocoder;

	function getParam(name)
    {  
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
        var regexS = "[\\?&]"+name+"=([^&#]*)";  
        var regex = new RegExp( regexS );  
        var results = regex.exec(window.location.href);
        if(results == null)
            return "";  
        else    
            return results[1];
    }
   function replaceAll(str, find, replace) {
 		return str.replace(new RegExp(find, 'g'), replace);
	}
   function setData()
   {
        latUrl = Number(getParam("lat"));
        lngUrl = Number(getParam("lng"));
        ad = String(decodeURIComponent(getParam("address")));
        addUrl = ad.replace(/\+/g," "); 
        
        distanceUrl = getParam("distance");
        var myLatLng = new google.maps.LatLng(latUrl,lngUrl);
        map.setCenter(myLatLng);   
         var marker = new google.maps.Marker({
	   			 position: myLatLng, map: map, title: "Here"  });
	   	marker.setMap(map);  
	   	$("#addin").text( addUrl);
	   	$("#simg").attr("src", ("http://maps.googleapis.com/maps/api/streetview?size=200x200&location=" + latUrl +","+ lngUrl +"&key=AIzaSyD4y9-ph5LOcR3tGyr6bkSQ6srnPdhB0KU&timestamp=" + new Date().getTime()));
	   	$("#btnnext").attr("href","risksearch3.html?address="+addUrl+"&lat="+latUrl+"&lng="+lngUrl);
	}
	
  function initMap() {
  	geocoder = new google.maps.Geocoder();
    latUrl = Number(getParam("lat"));
    lngUrl = Number(getParam("lng"));
    distanceUrl = getParam("distance");
    var x = "latUrl = "+latUrl+";lngUrl = "+ lngUrl + "; distance = " + distanceUrl;
    var myLatLng = new google.maps.LatLng(latUrl,lngUrl);
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: latUrl, lng: lngUrl},
      zoom: 18
    });
    map.setCenter(myLatLng);   
    skymap = new google.maps.Map(document.getElementById('skymap'), {
      center: {lat: latUrl, lng: lngUrl},
      mapTypeId: 'satellite',
      zoom: 18
    });
    var marker = new google.maps.Marker({
	   			 position: myLatLng, map: map, title: "Here"  });
	marker.setMap(map);  
	var parcelUrl = "http://115.146.84.245:8080/geoserver/wfs?service=wfs&version=1.0.0&request=GetFeature&typeName=IDDSS:parcel_view_vic&outputFormat=json&cql_filter=Intersects(geom,POINT("+lngUrl+" "+ latUrl+"))";
	setPolygon(parcelUrl,"parcel");
 
  }

function setPolygon(url,type){
  	  	 $.getJSON(url, function(result){
            var risks =  jQuery.parseJSON(JSON.stringify(result));
            if(risks.features[0]==null){ 
            	console.log("No Parcel");
            }else{
            	var geo = risks.features[0].geometry.coordinates[0][0];
            	var i;
            	var riskCTB=[];
            	for(i = 0; i< geo.length ; i++  ){
            		riskCTB[i] = new google.maps.LatLng(geo[i][1],geo[i][0]);
            		
            	}
            	if(type == "parcel")
            	{
            		parcelPolygon = new google.maps.Polygon({
		            path: riskCTB,
		            strokeColor: '#0080ff',
		            strokeOpacity: 0.8,
		            strokeWeight: 1,
		            fillOpacity: 0.1,
		            fillColor: '#0080ff',
		          	fillOpacity: 0.35
		        });
		        parcelPolygon.setMap(map);
            	}
            	
            }
          });
  }