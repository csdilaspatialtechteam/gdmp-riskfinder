var latUrl;
var lngUrl;
var distanceUrl;
var map;
var geocoder;
var fire = true;
var flood = true;
var heritage = true;
	function getParam(name)
    {  
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");  
        var regexS = "[\\?&]"+name+"=([^&#]*)";  
        var regex = new RegExp( regexS );  
        var results = regex.exec(window.location.href);
        if(results == null)
            return "";  
        else    
            return results[1];
    }
   function setData()
   {
        latUrl = Number(getParam("lat"));
        lngUrl = Number(getParam("lng"));
        ad = String(decodeURIComponent(getParam("address")));
        addUrl = ad.replace(/\+/g," "); 
        
        distanceUrl = getParam("distance");
        var myLatLng = new google.maps.LatLng(latUrl,lngUrl);
        map.setCenter(myLatLng);   
         var marker = new google.maps.Marker({
	   			 position: myLatLng, map: map, title: "Here"  });
	   	marker.setMap(map);  
	   	initPolygon();
	   	$("#addin").text(addUrl);
	   	$("#btnnext").attr("href","risksearch4.html?address="+addUrl+"&lat="+latUrl+"&lng="+lngUrl);
		console.log(flood)
   }

  function initMap() {
  	geocoder = new google.maps.Geocoder();
    latUrl = Number(getParam("lat"));
    lngUrl = Number(getParam("lng"));
    distanceUrl = getParam("distance");
    var x = "latUrl = "+latUrl+";lngUrl = "+ lngUrl + "; distance = " + distanceUrl;
    var myLatLng = new google.maps.LatLng(latUrl,lngUrl);
    map = new google.maps.Map(document.getElementById('map'), {
      center: {lat: latUrl, lng: lngUrl},
      zoom: 18
    });
    map.setCenter(myLatLng);   
    var marker = new google.maps.Marker({
	   			 position: myLatLng, map: map, title: "Here"  });
	marker.setMap(map);  
 
  }

  function initPolygon(){

  	
  	var fireUrl = "http://115.146.84.245:8080/geoserver/wfs?service=wfs&version=1.0.0&request=GetFeature&typeName=IDDSS:vfrr_assets_human_settlement_polygon&outputFormat=json&cql_filter=Intersects(wkb_geometry,POINT("+lngUrl+" "+ latUrl+"))";
  	var waterUrl = "http://115.146.84.245:8080/geoserver/wfs?service=wfs&version=1.0.0&request=GetFeature&typeName=IDDSS:flood_extent_100y&outputFormat=json&cql_filter=Intersects(geom,POINT("+lngUrl+" "+ latUrl+"))";
  	var overlayUrl = "http://115.146.84.245:8080/geoserver/wfs?service=wfs&version=1.0.0&request=GetFeature&typeName=IDDSS:plan_overlay_vic&outputFormat=json&cql_filter=Intersects(geom,POINT("+lngUrl+" "+ latUrl+"))";
  	var parcelUrl = "http://115.146.84.245:8080/geoserver/wfs?service=wfs&version=1.0.0&request=GetFeature&typeName=IDDSS:parcel_view_vic&outputFormat=json&cql_filter=Intersects(geom,POINT("+lngUrl+" "+ latUrl+"))";
  	var output = "test result:";
		setPolygon(fireUrl,"fire");
		setPolygon(waterUrl,"water");
		setPolygon(overlayUrl,"overlay");
		setPolygon(parcelUrl,"parcel");
	
  }
  
  function setTable()
  {
  	console.log(fire,flood,heritage);
  		if (!(fire&flood&heritage)){
  			$("#norisk").css("display","inline");
  			$("#risktable").css("display","none");
  			$("#riskinfo").css("display","none");
  			console.log("no risk");
  			return;
  		}
  		if(!fire){
  			$("#firetable").css("display","none");
  			$("#fireinfo").css("display","none");
  			console.log("fire risk");
  		}
  		if(!flood){
  			$("#floodtable").css("display","none");
  			$("#floodinfo1").css("display","none");
  			$("#floodinfo1").css("display","none");
  		}
  		if(!heritage){
  			$("#heritagetable").css("display","none");
  			$("#heritageinfo").css("display","none");
  		}
  		
  }
  function setPolygon(url,type){
  	  	 $.getJSON(url, function(result){
            var risks =  jQuery.parseJSON(JSON.stringify(result));
            if(risks.features[0]==null){ 
            	if(type == "fire")
            	{
            		$("#fireCheck").attr("disabled",true);
            		console.log("No Fire Risk");
            		$("#firetable").css("display","none");
  					$("#fireinfo").css("display","none");
            	}else if( type == "water")
            	{
            		$("#floodCheck").attr("disabled",true);
            		console.log("No Flood Risk");
            		$("#floodtable").css("display","none");
  					$("#floodinfo1").css("display","none");
  					$("#floodinfo2").css("display","none");
            	}else if(type == "overlay"){
            		$("#overlayCheck").attr("disabled",true);
            		$("#heritagetable").css("display","none");
  					$("#heritageinfo").css("display","none");
            	}
            }else{

            	var geo = risks.features[0].geometry.coordinates[0][0];
            	var i;
            	var riskCTB=[];
            	for(i = 0; i< geo.length ; i++  ){
            		riskCTB[i] = new google.maps.LatLng(geo[i][1],geo[i][0]);
            		
            	}
            	if(type == "fire")
            	{
            		$("#fireLevel").text(risks.features[0].properties.riskrating);
            		firePolygon = new google.maps.Polygon({
		            path: riskCTB,
		            strokeColor: '#FF0000',
		            strokeOpacity: 0.8,
		            strokeWeight: 1,
		            fillOpacity: 0.1,
		            fillColor: '#FF0000',
		          	fillOpacity: 0.35
		        });

            	}else if(type == "water")
            	{
            		floodPolygon = new google.maps.Polygon({
		            path: riskCTB,
		            strokeColor: '#0000FF',
		            strokeOpacity: 0.8,
		            strokeWeight: 1,
		            fillOpacity: 0.1,
		            fillColor: '#0000FF',
		          	fillOpacity: 0.35
		        });

            	}else if(type == "overlay")
            	{
            		overlayPolygon = new google.maps.Polygon({
		            path: riskCTB,
		            strokeColor: '#66ffc2',
		            strokeOpacity: 0.8,
		            strokeWeight: 1,
		            fillOpacity: 0.1,
		            fillColor: '#66ffc2',
		          	fillOpacity: 0.35
		        });
            	}
            	else if(type == "parcel")
            	{
            		parcelPolygon = new google.maps.Polygon({
		            path: riskCTB,
		            strokeColor: '#0080ff',
		            strokeOpacity: 0.8,
		            strokeWeight: 1,
		            fillOpacity: 0.1,
		            fillColor: '#0080ff',
		          	fillOpacity: 0.35
		        });
		        parcelPolygon.setMap(map);
            	}
            	if(type!="parcel")
            	{
					$("#norisk").css("display","none");
					$("#risktable").css("display","");
					$("#riskinfo").css("display","");
				}
            	
            }
          });
  }

  function toggleLayer(firLayer,id)
    {
        if ($('#'+id).is(':checked')) {
            firLayer.setMap(map);
        }
        else
        {
            firLayer.setMap(null);
        }
    }
