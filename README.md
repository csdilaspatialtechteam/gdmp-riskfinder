# README #

Welcome to the RiskFinder, a system set up to help land owners and occupiers become aware of and understand different threat that may affect a property. 

The purpose of this project is to facilitate the awareness of these threats to enable and facilitate further risk management to take place. 

The RiskFinder tool can help you search specific addresses to determine whether any risks or threats have been identified as affecting that parcel of land

### Versions ###

v1.0 (2016-11-29) 

This initial version was written by Dr. Yinyu Lu (luyinyu@gmail.com)


### Who do I talk to? ###

Dr. Katie Potts

Centre for Disaster Management and Public Safety

The University of Melbourne, Victoria 3010 Australia

kepotts@unimelb.edu.au


Dr. Yiqun (Benny) Chen

Centre for Disaster Management and Public Safety

The University of Melbourne, Victoria 3010 Australia

yiqun.c@unimelb.edu.au